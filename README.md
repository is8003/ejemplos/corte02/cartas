# Cartas

Oscar está jugando el siguiente juego. Él tiene un mazo de N cartas (numeradas
de 1 a N) sin números repetidos. El mazo ha sido barajado. Oscar comienza a
recorrer el mazo buscando la carta marcada con el número 1, después él busca la
carta marcada con el número 2 y así hasta llegar a la carta marcada con el
número N. Cada vez encuentra una carta, continua la búsqueda de la siguiente
carta desde el lugar en donde encontró la carta anterior. Cuando él llega al
final del mazo da un aplauso y comienza nuevamente al principio del mazo. Este
procedimiento terminará una vez que la última carta (la numerada con N) haya
sido encontrada.

Escriba una función llamada `recorrer` que, dado el mazo de cartas barajado con
el que Oscar comienza el juego, el cual se recibe en un vector `cartas` que
contiene los números de las cartas dados en el orden de aparición en el mazo
barajado, y el número de cartas `n` (`1 <= n <= 100`) encuentre el número de
veces en que Oscar aplaudirá durante el juego. La función debe devolver el
número de aplausos que Oscar dará al jugar con ese mazo.

## Refinamiento 1

### Función: `recorrer`

Retorna la cantidad de recorridos completados sobre vector `cartas` de tamaño
`n` hasta encontrar de manera ascendente todos los números desde `1` hasta `n`.

#### Entradas:

- `cartas`: arreglo de números enteros positivos entre `1` y `n` en orden
  aleatorio.
- `n`: tamaño del vector `cartas`.

#### Salidas:

- `aplausos`: cantidad de recorridos realizados sobre arreglo `cartas`.

#### Pseudo-código:

1. Declaro variable `aplausos` y asigno valor a cero.
1. Declaro variable para el elemento buscado `e` y asígno el valor de `1`.
1. Para `i` desde `0`; `e` menor o igual a `n`; asigno a `i` el módulo `n` de
   su incremento:
   1. Si `e` es igual a elemento en la posición `i` del arreglo `cartas`:
      1. Icremento `e` en `1`. 
   1. Si `i` es igual a `n - 1`:
      1. Incremento `aplausos` en `1`.
1. Retorno `aplausos`
