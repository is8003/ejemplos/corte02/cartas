#include <stdio.h>
#define CANT_CARTAS 7

unsigned recorrer(unsigned cartas[], unsigned n);

int main(void){
	unsigned mazo[CANT_CARTAS] = {3, 6, 7, 1, 5, 4, 2};

	printf("Aplausos: %u\n", recorrer(mazo, CANT_CARTAS));
}


/* ### Función: `recorrer`
 * 
 * Retorna la cantidad de recorridos completados sobre vector `cartas` de
 * tamaño `n` hasta encontrar de manera ascendente todos los números desde `1`
 * hasta `n`.
 * 
 * #### Entradas:
 * 
 * - `cartas`: arreglo de números enteros positivos entre `1` y `n` en orden
 *   aleatorio.
 * - `n`: tamaño del vector `cartas`.
 * 
 * #### Salidas:
 * 
 * - `aplausos`: cantidad de recorridos realizados sobre arreglo `cartas`.
 */ 
unsigned recorrer(unsigned cartas[], unsigned n){
	unsigned aplausos = 0;
	unsigned e = 1;

	for (unsigned i = 0; e <= n; i = (i + 1) % n){
		if (e ==  cartas[i]){
			e++;
		}

		if (i == n - 1){
			aplausos++; 
		} 
	}

	return aplausos; 
}
